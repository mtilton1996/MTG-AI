import CreatureCardFunctions as ccf
import types

def basicLandParser(card_name, card_type, card_subtype, Card):
    card = Card()
    card = Card()
    card.Name = card_name
    card.Type = card_type
    card.SubType = card_subtype
    if (card_name is not None):
        card.getName = types.MethodType(ccf.getName, card)
    if (card_type is not None):
        card.getType = types.MethodType(ccf.getType, card)
    if (card_subtype is not None):
        card.getSubType = types.MethodType(ccf.getSubtype, card)

    return card
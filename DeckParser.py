from CardDefinitionParser import main as CardParser

def scanfolder(name):
    import os
    for path, dirs, files in os.walk('Cards/'):
        for f in files:
            if f.endswith(name + '.cd'):
                return os.path.join(path, f)
    return None

def main(deck_name):
    # still needs to check to see if there are too many of a card
    deck_list = open('Decks/' + deck_name + '.dl', "r").read()
    deck_list = deck_list.split('\n')
    deck_list = [x for x in deck_list if x != '']
    if len(deck_list) < 60:
        return 'Invalid Deck: ' + str(len(deck_list)) + ' is less than 60 cards'
    deck = []
    for each in deck_list:
        card_path = scanfolder(each)
        if card_path is None:
            return 'Ivalid Deck: card ' + each + ' was not found'
        # print(type(CardParser(card_path)))
        deck.append(CardParser(card_path))
    return deck

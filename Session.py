import socket

class Session:
    def __init__(self, address):
        self.address = address # tuple (addr, port)
        self.socket = socket.socket()

    def connect(self):
        self.socket.connect(self.address)
    
    def isConnected(self):
        if self.socket.getpeername() != None:
            return True
        else:
            return False
    
    def send(self, data):
        self.socket.sendall("\n")
        for each in self.socket.recv(102400):
            print each
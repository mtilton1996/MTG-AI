import CreatureCardFunctions as ccf
import types

def creatureParser(card_name, card_cost, card_type, card_subtype, card_rarity, card_power, card_toughness, card_description, card_expansion, Card):
    card = Card()
    card.Name = card_name
    card.Cost = card_cost
    card.Type = card_type
    card.SubType = card_subtype
    card.Rarity = card_rarity
    card.Power = card_power
    card.Toughness = card_toughness
    card.Health = card_toughness
    card.Description = card_description
    card.Expansion = card_expansion
    if (card_name is not None):
        card.getName = types.MethodType(ccf.getName, card)
    if (card_cost is not None):
        card.getCost = types.MethodType(ccf.getCost, card)
    if (card_type is not None):
        card.getType = types.MethodType(ccf.getType, card)
    if (card_subtype is not None):
        card.getSubType = types.MethodType(ccf.getSubtype, card)
    if (card_rarity is not None):
        card.getRarity = types.MethodType(ccf.getRarity, card)
    if (card_power is not None):
        card.getPower = types.MethodType(ccf.getPower, card)
    if (card_toughness is not None):
        card.getToughness = types.MethodType(ccf.getToughness, card)
        card.setHealth = types.MethodType(ccf.setHealth, card)
        card.heal = types.MethodType(ccf.heal, card)
    if (card_description is not None):
        card.getDescription = types.MethodType(ccf.getDescription, card)
    if (card_expansion is not None):
        card.getExpansion = types.MethodType(ccf.getExpansion, card)

    return card
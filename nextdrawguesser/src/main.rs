use std::collections::HashMap;
// Takes in the players graveyard, face-up exiled cards. known cards in deck, known cards in hand, and cards on battlefield to calculate the likely next draw

// #[derive(Debug)]
// struct DeckCard {
//     name: String,
//     location: u8
// }

fn main() {
    let mut x = Deck::new();
    x.add_card(Card{name: CardName::LoomingAltisaur, location: -1});
    x.add_card(Card{name: CardName::LoomingAltisaur, location: -1});
    x.add_card(Card{name: CardName::Plains, location: -1});
    x.add_card(Card{name: CardName::Plains, location: -1});
    x.add_card(Card{name: CardName::Plains, location: -1});
    x.add_card(Card{name: CardName::Plains, location: -1});
    x.remove_card(CardName::LoomingAltisaur);
    x.calc_card_chance()
}

#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Clone)]
#[derive(Hash)]
#[derive(Eq)]
enum CardName {
    LoomingAltisaur,
    Plains
}

#[derive(Debug)]
struct Deck {
    cards: Vec<Card>
}

#[derive(Debug)]
struct Card {
    name: CardName,
    location: i16
}

impl Deck {
    fn add_card(&mut self, card: Card) {
        self.cards.push(card);
    }
    fn remove_card(&mut self, card_name: CardName) {
        let mut new_cards: Vec<Card> = Vec::new();
        for card in self.cards.iter().enumerate() {
            if card.1.name == card_name {
                let new_cards_first_half = &self.cards[0 as usize..card.0 as usize];
                let new_cards_second_half = &self.cards[(card.0 as u32 + 1) as usize..self.cards.len() as usize];
                for element in new_cards_first_half.iter() {
                    let card = Card {
                        name: element.name.clone(),
                        location: element.location
                    };
                    new_cards.push(card)
                }
                for element in new_cards_second_half.iter() {
                    let card = Card {
                        name: element.name.clone(),
                        location: element.location
                    };
                    new_cards.push(card)
                }
                break;
            }
        }
        self.cards = new_cards;
    }
    fn new() -> Deck {
        Deck {
            cards: Vec::new()
        }
    }
    fn calc_card_chance(&self) {
        let mut card_names_counts: HashMap<&CardName, u16> = HashMap::new();
        for card in self.cards.iter() {
            let count = match card_names_counts.get(&card.name) {
                Some(value) => value + 1,
                None => 1,
            };
            card_names_counts.insert(&card.name, count);
        }
        println!("{:?}", card_names_counts.get(&CardName::Plains));
    }
}
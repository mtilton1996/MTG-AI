class Card:
    # Cost should be in the format 
    # C for colorless
    # W for white
    # B for black
    # U for blue
    # R for red
    # G for green
    # 1C3U is one colorless and 3 blue
    # Type is one of Creature, Sorcery, Instant, or Artifact
    # Power should be an integer
    # Toughness should be an integer
    # abilities is an array of abilities
    def __init__(self, cost, card_type, power, toughness, abilities):
        self.cost = cost
        self.card_type = card_type
        self.power = power
        self.toughness = toughness
        self.abilities = abilities
    
    def get_toughness(self):
        return self.toughness

    def get_power(self):
        return self.power
    
    def get_cost(self):
        return self.cost
    
    def get_card_type(self):
        return self.card_type
    
    def get_abilities(self):
        return self.abilities
        
    
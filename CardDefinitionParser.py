from CardCreatureParser import creatureParser
from CardBasicLandParser import basicLandParser

class Card:
    pass

RESERVED_WORDS = ['Name: ', 'Cost: ', 'Type: ', 'SubType: ', 'Rarity: ', 'Power: ', 'Toughness: ', 'Description: ', 'Loyalty: ', 'Expansion: ']


def split_cd_by_reserved_word(cd, rw):
    try:
        return cd.split(RESERVED_WORDS[rw])[1].split('\n')[0]
    except:
        return None

def main(path):
    card_definition = open(path, "r").read()
    card_name = split_cd_by_reserved_word(card_definition, 0)
    card_cost = split_cd_by_reserved_word(card_definition, 1)
    card_type = split_cd_by_reserved_word(card_definition, 2)
    card_subtype = split_cd_by_reserved_word(card_definition, 3)
    card_rarity = split_cd_by_reserved_word(card_definition, 4)
    card_power = split_cd_by_reserved_word(card_definition, 5)
    card_toughness = split_cd_by_reserved_word(card_definition, 6)
    card_description = split_cd_by_reserved_word(card_definition, 7)
    card_expansion = split_cd_by_reserved_word(card_definition, 9)
    if (card_type == 'Creature'):
        return creatureParser(card_name, card_cost, card_type, card_subtype, card_rarity, card_power, card_toughness, card_description, card_expansion, Card)
    if (card_type == 'Basic Land'):
        return basicLandParser(card_name, card_type, card_subtype, Card)

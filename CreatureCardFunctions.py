
def getName(self):
    return self.Name

def getCost(self):
    return self.Cost

def getType(self):
    return self.Type

def getSubtype(self):
    return self.SubType

def getRarity(self):
    return self.Rarity

def getPower(self):
    return self.Power

def getToughness(self):
    return self.Toughness

def getDescription(self):
    return self.Description

def getExpansion(self):
    return self.Expansion

def setHealth(self, newHealth):
    self.Health = newHealth

def heal(self):
    self.Health = self.Toughness